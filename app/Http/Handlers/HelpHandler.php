<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 8/27/2017
 * Time: 8:50 AM
 */

namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;


class HelpHandler extends BaseHandler{
	/**
	 * If this function returns true, the handle method will get called.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return bool
	 */
	public function canHandle(Request $request): bool
	{
		$questions = false;
		if(starts_with($request->text, 'what can you do')
			|| starts_with($request->text, 'who are you')
			|| starts_with($request->text, 'help')
			|| starts_with($request->text, '')
		) $questions = true;

		return $questions;
	}

	/**
	 * Handle the given request.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return \Spatie\SlashCommand\Response
	 */
	public function handle(Request $request): Response
	{

		$message = "Hi there! My name is Giana, SV Chrome's Smart Assistant from hotelo. 
		Im currently a prototype but i can already help you make your stay better. I can help you make complaints or request for services and you can ask me things like 
		 'What is on the menu?',
		 'Where can i go?',
		 'What is on the news?' and more...";


		return $this->respondToSlack($message)
//		            ->withAttachment(Attachment::create()
//                    ->setColor('good')
//                    ->setText("To send a message to the manager for example, just type `/giana sms manager Good day manager!`"))
		            ->withAttachment(Attachment::create()
                    ->setColor('good')
                    ->setText("To find out whats on the menu, just type `/giana what is on the menu?`"))

		            ->withAttachment(Attachment::create()
                    ->setColor('good')
                    ->setText("To find out where you should check out, just type `/giana where can i go?`"))

					->withAttachment(Attachment::create()
                    ->setColor('good')
                    ->setText("And to get the news just type `/giana news update`"))

					->withAttachment(Attachment::create()
                    ->setColor('good')
                    ->setText("And to get the news just type `/giana news update`"))

					->withAttachment(Attachment::create()
		                           ->setColor('good')
		                           ->setText("To get a department's phone number (Eg. Toby) just type `/giana phone kitchen`"));

	}

}


