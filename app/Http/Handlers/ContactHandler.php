<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 8/27/2017
 * Time: 8:50 AM
 */

namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;


class ContactHandler extends BaseHandler{
	/**
	 * If this function returns true, the handle method will get called.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return bool
	 */
	public function canHandle(Request $request): bool
	{
		return starts_with($request->text, 'phone');
	}

	/**
	 * Handle the given request.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return \Spatie\SlashCommand\Response
	 */
	public function handle(Request $request): Response
	{

		// format for message is sms phone message
		$text = explode(" ", $request->text);

		$phone = $text[1];

		switch(strtolower($phone)){
			case "toby":
				$phone = "07056468294";
				break;
			case "kitchen":
				$phone = "3022";
				break;
			case "sunny":
				$phone = "08120522261";
				break;
			case "sarah":
				$phone = "08118770031";
				break;
			case "richard":
				$phone = "08068168753";
				break;
			case "dexter":
				$phone = "07013024291";
				break;
			default;
		}


		return $this->respondToSlack("Here is $text[1]'s phone number $phone");
	}


}


