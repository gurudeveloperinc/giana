<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 8/27/2017
 * Time: 8:50 AM
 */

namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;


class NewsHandler extends BaseHandler{
	/**
	 * If this function returns true, the handle method will get called.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return bool
	 */
	public function canHandle(Request $request): bool
	{
		return starts_with($request->text, 'news');
	}

	/**
	 * Handle the given request.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return \Spatie\SlashCommand\Response
	 */
	public function handle(Request $request): Response
	{

		$bbcXML = "http://feeds.bbci.co.uk/news/rss.xml?edition=uk";
		$bbcNews = simplexml_load_file($bbcXML);
		$namespace = "http://search.yahoo.com/mrss/";

		$response = "";
		foreach ($bbcNews->channel->item as $rss) {
			$title          = $rss->title;
			$description    = $rss->description;
			$link           = $rss->link;
			$date_raw       = $rss->pubDate;
			$date           = date("Y-m-j G:i:s", strtotime($date_raw));
			$image = $rss->children($namespace)->thumbnail->attributes();

			$response .= $title . "\n";
			$response .= $description ."\n\n";
			$response .= $image["url"] . "\n\n";

		}


		return $this->respondToSlack("Here's the latest news\n$response");
	}

}


