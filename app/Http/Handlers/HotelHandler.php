<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 8/27/2017
 * Time: 8:50 AM
 */

namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Attachment;
use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;


class HotelHandler extends BaseHandler{
	/**
	 * If this function returns true, the handle method will get called.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return bool
	 */
	public function canHandle(Request $request): bool
	{
		$questions = false;
		if(starts_with(strtolower($request->text), 'what is on the menu')
		   || starts_with(strtolower($request->text), 'where can i go')
		) $questions = true;

		return $questions;
	}

	/**
	 * Handle the given request.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return \Spatie\SlashCommand\Response
	 */
	public function handle(Request $request): Response
	{
		if(starts_with(strtolower($request->text), 'what is on the menu')){

			return $this->respondToSlack("For lunch today, we have french fries, pounded yam, fried rice, jollof rice and shrimps. Which would you like?");
		} else if(starts_with(strtolower($request->text), 'where can i go')) {
			return $this->respondToSlack("You can check out Jabi Lake Mall, It's about 1km from here. Click this link for directions on how to get there. :) https://goo.gl/maps/rZx7xjartYr ");

			}


	}

}


