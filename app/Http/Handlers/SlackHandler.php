<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 8/27/2017
 * Time: 8:50 AM
 */

namespace App\SlashCommandHandlers;

use Spatie\SlashCommand\Request;
use Spatie\SlashCommand\Response;
use Spatie\SlashCommand\Handlers\BaseHandler;


class SlackHandler extends BaseHandler{
	/**
	 * If this function returns true, the handle method will get called.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return bool
	 */
	public function canHandle(Request $request): bool
	{
		return starts_with($request->text, 'sms');
	}

	/**
	 * Handle the given request.
	 *
	 * @param \Spatie\SlashCommand\Request $request
	 *
	 * @return \Spatie\SlashCommand\Response
	 */
	public function handle(Request $request): Response
	{

		// format for message is sms phone message
		$text = explode(" ", $request->text);

		$phone = $text[1];

		switch(strtolower($phone)){
			case "toby":
				$phone = "08081234504";
				break;
			case "sam":
				$phone = "08064257998";
				break;
			case "sunny":
				$phone = "08120522261";
				break;
			case "sarah":
				$phone = "08118770031";
				break;
			case "richard":
				$phone = "08068168753";
				break;
			case "dexter":
				$phone = "07013024291";
				break;
			default;
		}

		$message = "";
		for($i = 2; $i <= count($text) - 1; $i++){
			$message .=  $text[$i] . " ";
		}

		$this->sendSms($phone,$message);
		return $this->respondToSlack("Your message was sent. The phone number is $phone and the message is $message");
	}

	function sendSms($phone,$Message){

		/* Variables with the values to be sent. */
		$owneremail="tobennaa@gmail.com";
		$subacct="clearance";
		$subacctpwd="clearance";
		$sendto= $phone; /* destination number */
		$sender="Giana"; /* sender id */

		$message= $Message;  /* message to be sent */

		/* create the required URL */
		$url = "http://www.smslive247.com/http/index.aspx?"  . "cmd=sendquickmsg"  . "&owneremail=" . UrlEncode($owneremail)
		       . "&subacct=" . UrlEncode($subacct)
		       . "&subacctpwd=" . UrlEncode($subacctpwd)
		       . "&message=" . UrlEncode($message)
		       . "&sender=" . UrlEncode($sender)
		       ."&sendto=" . UrlEncode($sendto)
		       ."&msgtype=0";


		/* call the URL */
		if ($f = @fopen($url, "r"))  {

			$answer = fgets($f, 255);

			if (substr($answer, 0, 1) == "+") {
//				 "SMS to $dnr was successful.";
			}
//			else  {
//				 "an error has occurred: [$answer].";  }
		}

		else  {   "Error: URL could not be opened.";  }
	}

}


